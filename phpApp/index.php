<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');


require __DIR__ . '../vendor/autoload.php';
require __DIR__ . '../vendor/yiisoft/yii2/Yii.php';

class GeneralWorker extends yii\web\Application
{	
	public function GeneralFunction($MyString, $MyInt) {
		echo $MyString;
		echo $MyInt;
	}

	public function GetString()
	{
		return "helloworld";
	}

	public function GetInt()
	{
		return 12;
	}
}


$config = require __DIR__ . '../config/web.php';

$worker = new GeneralWorker($config);
$worker->GeneralFunction("helloworld", 12);

$worker->run();

